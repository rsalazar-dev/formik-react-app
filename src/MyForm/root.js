
import ReactReduxFactory from "../hoc/ReactReduxFactory";
import * as actions from "./actions";
import reducers from "./reducers";
import MyForm from "../components/MyForm";

const MyFormRoot = ReactReduxFactory({
    actions,
    reducers,
    view: MyForm
});

export default MyFormRoot;