import { combineReducers } from "redux";

const testData = (state = "", {type, payload}) => {
    switch(type) {
        case "TEST_ACTION":
            return payload;
        default:
            return state;
    }
}

const toDateMin = (state = "", {type, payload}) => {
    switch(type) {
        case "SET_TO_DATE_MIN":
            return payload;
        default:
            return state;
    }
}

const showDate = (state = "no-date", {type, payload}) => {
    switch(type) {
        case "ENABLE_DATE":
            return payload;
        default:
            return state;
    }
}

const reducers = combineReducers({
    testData,
    toDateMin,
    showDate
});

export default reducers;