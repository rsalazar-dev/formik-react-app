const setToDateMin = (payload) => ({
    type: "SET_TO_DATE_MIN",
    payload
});

const enableDate = (payload) => ({
    type: "ENABLE_DATE",
    payload
});

export {
    setToDateMin,
    enableDate
};