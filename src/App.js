import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import MyFormRoot from './MyForm/root';

class App extends Component {
  render() {
    return (
      <div className="App">
        <MyFormRoot/>
      </div>
    );
  }
}

export default App;
