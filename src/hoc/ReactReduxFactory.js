
import { createStore, applyMiddleware, compose } from "redux";
import { Provider, connect } from "react-redux";
import thunkMiddleware from "redux-thunk";
import React from "react";

const ReactReduxFactory = ({ reducers, view, actions, mapStateToProps, CWMCallback }) => {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    let additionalProps = {};
    const customStore = createStore(reducers, /* preloadedState, */ composeEnhancers(
        applyMiddleware(thunkMiddleware),
    ));

    const defaultMapping = (state) => {
        return { ...state, ...additionalProps }; // copy props from react-on-rails
    };

    mapStateToProps = mapStateToProps || defaultMapping;

    const VisibleComponent = connect(
        mapStateToProps,
        actions
    )(view);

    return class extends React.Component {
        constructor(props) {
            super(props);
            this.defaultCWM = this.defaultCWM.bind(this);
            additionalProps = { ...props };
            const initializeStore = CWMCallback || this.defaultCWM;
            initializeStore(this);
        }

        defaultCWM() {
            this.store = customStore;
        }

        render() {
            return (
                <Provider store={this.store}>
                    <VisibleComponent  />
                </Provider>
            );
        }
    };
};

export default ReactReduxFactory;