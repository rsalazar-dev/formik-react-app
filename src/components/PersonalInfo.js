import React from 'react';
import Shore from "./Shore";
import { FastField } from 'formik';

const checkWarning = (errors, touched) => touched ?
    (errors ? "warning" : "green")
        : "";

class PersonalInfo extends React.Component {
    render() {
        const { errors, touched } = this.props;
        return (
            <Shore.Section>
                <h1>Personal Information</h1>
                <hr height="1px" style={{
                    border: "none",
                    borderBottom: "1px solid rgba(0,0,0,0.12)",
                    }}/>
                <br/>
                <Shore.FormLabel context={"First Name: "} fieldName="firstname" {...this.props}>
                    <FastField
                            type="text"
                            name="firstname"
                            className={ checkWarning(errors.firstname, touched.firstname) }
                        />
                </Shore.FormLabel>
                <Shore.FormLabel context={"Last Name: "} fieldName="lastname" {...this.props}>
                    <FastField
                            type="text"
                            name="lastname"
                            className={ checkWarning(errors.lastname, touched.lastname) }
                        />
                </Shore.FormLabel>
                <Shore.FormLabel context={"Birth Date: "} fieldName="birthdate" {...this.props}>
                    <FastField
                            type="date"
                            name="birthdate"
                            className={ checkWarning(errors.birthdate, touched.birthdate) }
                        />
                </Shore.FormLabel>
            </Shore.Section>
        )
    }
}

export default PersonalInfo;