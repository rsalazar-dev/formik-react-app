import React from 'react';

const FormLabel = ({ errors, touched, fieldName }) => (
    <span className="shore form-label">
        {
            errors[fieldName] &&
            touched[fieldName] &&
            errors[fieldName].toString()
        }
        &nbsp;
    </span>
);

export default FormLabel;