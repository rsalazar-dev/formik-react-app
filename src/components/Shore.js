import React from "react";
import "../stylesheets/Shore.css";
import FormLabel from "./FormLabel";

const Shore = {
    Popper:({ context, children }) => (
        <div className="shore popper">
            <div className="wrapper">
                <p> { context || children } </p>
                </div>
            <i className="pointer"></i>
        </div>
    ),
    Container: ({children}) => (
        <div className="shore container">
            {children}
        </div>
    ),
    Section: ({children}) => (
        <section className="shore container">
            {children}
        </section>
    ),
    FormLabel: ({context, fieldName, children, errors, touched}) => (
        <label className="shore form-label">
            <span className="actual-label"> {context} </span>
            {children}
            <FormLabel
                errors={errors}
                touched={touched}
                fieldName={fieldName}/>
        </label>
    ),

}

export default Shore;