import React from 'react';
import { Formik, Form, FastField } from 'formik';
import PersonalInfo from "./PersonalInfo";
import FormLabel from "./FormLabel";
import "../stylesheets/Shore.css";
import "../stylesheets/MyForm.css";
import FormikEffect from "./FormikEffect";
import MyFormSchema from "../FormSchema/MyFormSchema";

const checkWarning = (errors, touched, className = "warning") => errors && touched && className;

const MyForm = (props) => {
    const {
        setToDateMin,
        toDateMin,
        enableDate,
        showDate
    } = props; // Actions and stores
    return (
        <div>
            <h1> Form validation using Formik</h1>
            <hr height="1px" style={{
                border: "none",
                borderBottom: "1px solid rgba(0,0,0,0.12)",
                }}/>
            <br/>
            <Formik
                initialValues={{
                    username: "NotSoLongUsername",
                    firstname: "John",
                    lastname: "Doe",
                }}
                validationSchema={MyFormSchema}
                onSubmit={(values, { setSubmitting }) => {
                        alert(JSON.stringify(values, null, 2));
                        setSubmitting(false);
                }}
            >
                {(props) => {
                    const {
                        handleSubmit,
                        isSubmitting,
                        errors,
                        touched,
                        setFieldValue,
                    } = props; // Formik Props

                    return (
                        <Form onSubmit={handleSubmit}>
                            <FormikEffect onChange={(prev, next) => {
                                if(prev.values.fromdate !== next.values.fromdate) {
                                    setToDateMin(next.values.fromdate);
                                    setFieldValue("todate", next.values.fromdate);
                                }
                                if(prev.values.enabledate !== next.values.enabledate) {
                                    enableDate(next.values.enabledate);
                                }

                            }}
                            />
                            <label className="shore form-label">
                                <span className="actual-label"> Username: </span>
                                <FastField
                                    type="text"
                                    name="username"
                                    className={ checkWarning(errors.username, touched.username) }
                                />
                                <FormLabel errors={errors} touched={touched} fieldName="username"/>
                            </label>
                            <br/>
                            <label className="shore form-label">
                                <span className="actual-label"> Password: </span>
                                <FastField
                                    type="text"
                                    name="password"
                                    className={ checkWarning(errors.password, touched.password) }
                                />
                                <FormLabel errors={errors} touched={touched} fieldName="password"/>
                            </label>
                            <label className="shore form-label">
                                <span className="actual-label"> Confirm: </span>
                                <FastField
                                    type="text"
                                    name="confirmpassword"
                                    className={ checkWarning(errors.confirmpassword, touched.confirmpassword) }
                                />
                                <FormLabel errors={errors} touched={touched} fieldName="confirmpassword"/>
                            </label>
                            <br/>
                            <label className="shore form-label">
                                    <span className="actual-label"> Enable Date: </span>
                                    <FastField component="select" name="enabledate"
                                    >
                                        <option value="no-date" label="No Date" />
                                        <option value="single-date" label="Single Date" />
                                        <option value="multi-date" label="Multi Date" />
                                    </FastField>
                            </label>
                            <br/>
                            {
                                /^(?:single-date|multi-date)$/.test(showDate) &&
                                <label className="shore form-label">
                                    <span className="actual-label"> From: </span>
                                    <FastField
                                        type="date"
                                        name="fromdate"
                                        className={ checkWarning(errors.fromdate, touched.fromdate) }
                                    />
                                    <FormLabel errors={errors} touched={touched} fieldName="fromdate"/>
                                </label>
                            }
                            {
                                showDate === "multi-date" &&
                                <label className="shore form-label">
                                    <span className="actual-label"> To: </span>
                                    <FastField
                                        type="date"
                                        name="todate"
                                        min={toDateMin}
                                        disabled={!toDateMin}
                                        className={ checkWarning(errors.todate, touched.todate) }
                                    />
                                    <FormLabel errors={errors} touched={touched} fieldName="todate"/>
                                </label>
                            }


                            <PersonalInfo {...props}/>
                            <br/>
                            <input type="submit"
                                disabled={
                                    isSubmitting ||
                                    Object.keys(errors).length > 0 ||
                                    Object.keys(touched).length === 0
                                }
                                value="Submit"
                            />
                        </Form>
                    );
                }}
            </Formik>
        </div>
    );
}

export default MyForm;