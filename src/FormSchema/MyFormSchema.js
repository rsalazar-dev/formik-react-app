import * as yup from "yup";

const yupAlphabetOnly = yup.string()
    .required("This is required")
    .test("AZaz09_", "Only accepts alphabet",
            (value) => value ?
                !value.match(/[^a-z ]/i) :
                false
        );

const MyFormSchema = yup.object().shape({
    username: yup.string()
        .required("This is required")
        .min(6, "Should be 6 to 20 characters long")
        .max(20, "Should be 6 to 20 characters long")
        .test("AZaz09_", "Only accepts alpha/numeric characters and underscores",
            (value) => value ?
                !value.match(/[^A-Za-z0-9_]/) :
                false
        ),
    password: yup.string()
        .required("This is required")
        .min(6, "Should be 6 to 20 characters long")
        .max(20, "Should be 6 to 20 characters long"),
    confirmpassword: yup.string()
        .oneOf([yup.ref("password"), null], "Password does not match")
        .required("This is required"),

    enabledate: yup.string(),
    fromdate: yup.date(),
    todate: yup.date(),
    firstname: yupAlphabetOnly,
    lastname: yupAlphabetOnly,
    birthdate: yup.date()
        .required("This is required")
        .max("2005-01-01", "Too young")
});

export default MyFormSchema;